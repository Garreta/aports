# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=khtml
pkgver=5.102.0
pkgrel=0
pkgdesc="The KDE HTML library, ancestor of WebKit"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later AND LGPL-2.1-only"
depends_dev="
	giflib-dev
	karchive-dev
	kcodecs-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kjs-dev
	knotifications-dev
	kparts-dev
	ktextwidgets-dev
	kwallet-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libjpeg-turbo-dev
	perl-dev
	qt5-qtbase-dev
	samurai
	sonnet-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	gperf
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/khtml-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2e280730cd234d48cc57959c4191b6cb827fc79ad24bdb3ed911c241df384aa57ec6060e54007fe31270bf49b263515ef288be1fc53e79f5b5fe775a9ffd3bfd  khtml-5.102.0.tar.xz
"
