# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdelibs4support
pkgver=5.102.0
pkgrel=0
pkgdesc="Porting aid from KDELibs4"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later AND MIT AND LGPL-2.1-only AND LGPL-2.0-only AND (LGPL-2.1-only OR LGPL-3.0-only) AND (LGPL-2.0-only OR LGPL-3.0-only)"
depends_dev="
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcrash-dev
	kdbusaddons-dev
	kded-dev
	kdesignerplugin
	kdesignerplugin-dev
	kdoctools-dev
	kemoticons-dev
	kglobalaccel-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kinit-dev
	kio-dev
	kitemmodels-dev
	knotifications-dev
	kparts-dev
	kservice-dev
	ktextwidgets-dev
	kunitconversion-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	perl-uri
	qt5-qtbase-dev
	qt5-qttools-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	kded
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kdelibs4support-$pkgver.tar.xz
	0001-fix-test-build.patch
	"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="suid !check" # Fails due to requiring running dbus

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f915e0c034ba3562a0636d3e79293c5e56d41f686b1375923fc2fbd6cfe884f81ce78c7aa85642071bf14d22b69fce7edf4f3736741602807032a15ecbfac3b0  kdelibs4support-5.102.0.tar.xz
a6359589e4d4fe71394dd9ccfd1910ee7fb20133b0032d24d4d8862e1900f61e1ac8ea059e734cde8ca5696abc970ebde54ba8fd92156c3de2ebdecbdbee7d8e  0001-fix-test-build.patch
"
