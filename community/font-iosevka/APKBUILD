# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=font-iosevka
pkgver=17.0.4
pkgrel=0
pkgdesc="Versatile typeface for code, from code"
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base
		$pkgname-slab
		$pkgname-curly
		$pkgname-curly-slab
	"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/iosevka.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/iosevka-curly-slab.ttc
}

sha512sums="
1bbfafd974ecff00edd920966566629d6b5d08152c846bb2103201fbfa12ea5bcb4404bd1a51d8dbee470f8b9584af5912e01b5683890a2b518f317dc025a3f2  super-ttc-iosevka-17.0.4.zip
033efb352616e6f54ed5f66bc0dc101e55bcf830a320bce0163eeccafa2efbe24c43b11f2c3af1ef6b8366d5309ede6bb27470b01041a5cabf322fb1964f27e5  super-ttc-iosevka-slab-17.0.4.zip
e57a6afceabd0a3fda1d6153301e401d00b888417a3c87a543598456e7e6e4bb3dc62f5efe67852f2fd7d8550a82c9006d8cba2cff4c0927f30cafbb67f88767  super-ttc-iosevka-curly-17.0.4.zip
b5a5dab7031bd687cb6e53762933cacf463209d92a70b9f8bfac36e3370f3620a057038b226af8e73c7011d9548da2e87323941c55a370a6e24531e0267ab25b  super-ttc-iosevka-curly-slab-17.0.4.zip
"
