# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=wt
pkgver=4.9.0
pkgrel=0
pkgdesc="C++ library and application server for developing and deploying web applications"
url="https://www.webtoolkit.eu/"
arch="all"
license="GPL-2.0-only WITH openssl-exception"
depends_dev="
	boost-dev
	fcgi-dev
	glu-dev
	graphicsmagick-dev
	harfbuzz-dev
	libharu-dev
	libpq-dev
	mesa-dev
	openssl-dev>3
	pango-dev
	qt5-qtbase-dev
	sqlite-dev
	zlib-dev
	"
makedepends="$depends_dev cmake samurai"
pkggroups="wt"
pkgusers="wt"
options="!check"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/emweb/wt/archive/$pkgver.tar.gz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCONNECTOR_HTTP=ON \
		-DWT_WRASTERIMAGE_IMPLEMENTATION=GraphicsMagick \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DWEBUSER=$pkgusers \
		-DWEBGROUP=$pkggroups \
		-DRUNDIR=/run/wt \
		-DUSE_SYSTEM_SQLITE3=ON \
		-DINSTALL_EXAMPLES=ON \
		-DBUILD_EXAMPLES=OFF
	cmake --build build
}

package() {
	DESTDIR=$pkgdir cmake --install build
	rm -rf $pkgdir/var/run
}

sha512sums="
6592d35505409c67d8463066b0ab3d0843f253ca62b31598437dc572e9360d79a7c130f86c60f9752cdc963eb4f4e7f5911d5fe37c40461b980a293b70640d05  wt-4.9.0.tar.gz
"
