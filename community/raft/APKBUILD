# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=raft
pkgver=0.16.0
pkgrel=0
pkgdesc="C implementation of the Raft consensus protocol"
url="https://github.com/canonical/raft"
arch="all"
license="LGPL-3.0-only WITH LGPL-3.0-linking-exception"
makedepends="linux-headers libuv-dev autoconf automake libtool"
subpackages="$pkgname-static $pkgname-dev $pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/canonical/raft/archive/v$pkgver.tar.gz
	unistd-include.patch
	disable-bind-address-test-segfaulting.patch"

prepare() {
	default_prepare
	autoreconf -i
}

build() {
	./configure \
		--prefix=/usr \
		--disable-lz4 \
		--enable-example=no
	make
}

check() {
	make check || {
		cat ./test-suite.log
		return 1
	}
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

sha512sums="
cee4a2f6fd9a0a16b591d46aa9df2104a50f2b62068eb27017e3347fc28a6b3cd3aef6bfabe3acc7e72844406b4b71aff1d1e088d08e83d6d2f5744876a681c8  raft-0.16.0.tar.gz
1e0e82e42fb9a65e6135e47ef17494e40f973c9a1af8aab09ff10fdbee83b55183414ec6938205806a9f954bbefaaa5eee776bbd313072ac7ce0105f18a2be03  unistd-include.patch
c5c1fafcd379ee9ecc8ccb9eb5e4b829ac9154270e3ebb041ee42ddb3a0ebec23ef5de41501d0225089012921d0d491d5d63e221b06c47b50cd74b09f39342d4  disable-bind-address-test-segfaulting.patch
"
